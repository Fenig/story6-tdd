from django import forms
from .models import StatusModel

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields = ('content',)
        widgets = {}