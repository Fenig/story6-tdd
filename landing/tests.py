'''
Learn test-driven-development with django
Gregorius Aprisunnea, 1706039710, PPW-A
'''
from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

from .views import (index, profile, landing_page_content, profile_name,
profile_npm, profile_hobby, profile_education, profile_experience)
from .models import StatusModel
from .forms import StatusForm

from django.utils import timezone

class ProfileUnitTest(TestCase):
    # check if the template is the intended
    def test_page_using_correct_template(self):
        response = Client().get(reverse('profile'))
        self.assertTemplateUsed(response, 'profile.html')

    # Check if url in "/profile" exists
    def test_profile_url_is_exist(self):
        response = Client().get(reverse('profile'))
        self.assertEqual(response.status_code,200)

    # Check if the function called for "/profile" is the profile function in views
    def test_profile_using_profile_func(self):
        found = resolve(reverse('profile'))
        self.assertEqual(found.func, profile)
    
    # check if the profile_name is written
    def test_landing_page_content_is_written(self):
        self.assertIsNotNone(profile_name) # Content cannot be null
        self.assertEqual(profile_name, "Gregorius Aprisunnea")
    
    # check if the profile_npm is written
    def test_profile_npm_is_written(self):
        self.assertIsNotNone(profile_npm) # Content cannot be null
        self.assertEqual(profile_npm, "1706039710")
    
    # check if the profile_hobby is written
    def test_profile_hobby_is_written(self):
        self.assertIsNotNone(profile_hobby) # Content cannot be null
        self.assertEqual(profile_hobby, "Makan")
    
    # check if the profile_education is written
    def test_profile_education_is_written(self):
        self.assertIsNotNone(profile_education) # Content cannot be null
        self.assertEqual(profile_education, "Universitas Indonesia")
    
    # check if the profile_experience is written
    def test_profile_experience_is_written(self):
        self.assertIsNotNone(profile_experience) # Content cannot be null
        self.assertEqual(profile_experience, "Expert in eating")

    # # check if landing page is in the way we want
    # def test_landing_page_is_completed(self):
    #     request = HttpRequest()
    #     response = profile(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(profile_name, html_response)
    #     self.assertIn(profile_npm, html_response)
    #     self.assertIn(profile_hobby, html_response)
    #     self.assertIn(profile_education, html_response)
    #     self.assertIn(profile_experience, html_response)

class IndexUnitTest(TestCase):
    # Check if url in "/" exists
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    # Check if the function called for "/" is the index function in views
    def test_index_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    # check if the llanding_page_content is written
    def test_landing_page_content_is_written(self):
        self.assertIsNotNone(landing_page_content) # Content cannot be null
        self.assertEqual(landing_page_content, "Hello, Apa kabar?")
    
    # check if landing page is in the way we want
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)

    # check if model can create new status
    def test_model_can_create_new_status(self):
        new_status = StatusModel.objects.create(content='ini content status')
        # Retrieving all available activity
        counting_all_available_status = StatusModel.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)
    
    # check if the created_At is null or not after insertion
    def test_status_time_is_inserted(self):
        new_status = StatusModel.objects.create(created_at=timezone.now())
        self.assertIsNotNone(new_status.created_at)

    # check if the template is the intended
    def test_page_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    # check if content > 300 do not go into db
    def test_form_more_300(self):
        data = {
            'content': ("String yang melebihi 300 karakter"
                        "CzzzzzzzzzzzzzzzzzzzzzzzzzzwfwfwezzzzzzzzzzzzzzzzzzE3hG7V"
                        "33333333q1lDRBGaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayQg"
                        "QrCInaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaauas3sadfevefrvezVp"
                        "aaaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsssssssssssrevffvevssssJs"
                        "sdvfvswfbgdvffcsdsxwkD11vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                        "sssssssssssssssssssvqxkddsf32frejaaaaaaaaaaaaaaaaaaaaaar")
        }
        response = Client().post('/', data=data)
        self.assertEqual(response.status_code, 200)

        status_list = StatusModel.objects.all()
        for status in status_list:
            self.assertNotEqual(status.content, data['content'])

class FormUnitTest(TestCase):
    # check if form is valid
    def test_form_is_valid(self):
        w = StatusModel.objects.create(content='test_valid_content')
        data = {'content': w.content}
        form = StatusForm(data=data)
        self.assertTrue(form.is_valid())

    # check if valid, submitting form content with >300 char
    def test_status_form_invalid_exceed_max_length(self):
        data = {
            'content': ("String yang melebihi 300 karakter"
                        "CzzzzzzzzzzzzzzzzzzzzzzzzzzwfwfwezzzzzzzzzzzzzzzzzzE3hG7V"
                        "33333333q1lDRBGaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayQg"
                        "QrCInaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaauas3sadfevefrvezVp"
                        "aaaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsssssssssssrevffvevssssJs"
                        "sdvfvswfbgdvffcsdsxwkD11vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                        "sssssssssssssssssssvqxkddsf32frejaaaaaaaaaaaaaaaaaaaaaar")
        }
        form = StatusForm(data=data)
        self.assertFalse(form.is_valid())


    # check if form is invalid
    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'content': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['content'],
            ["This field is required."]
        )
    
    # check if form create object successfully
    def test_form_successfully_create_object(self):
        form = StatusForm({'content': 'status content-form successfully create object?'})
        self.assertTrue(form.is_valid())
        status = form.save()
        self.assertEqual(status.content, 'status content-form successfully create object?')


    # check if client can post and retrieve the post
    def test_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'content': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

class StatusFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest,self).setUp()

    def tearDown(self):
        time.sleep(6)
        self.selenium.quit()
        super(StatusFunctionalTest,self).tearDown()

    def test_status_form_can_submit(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000')
        # find the form element
        content = selenium.find_element_by_name('content')
        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        status_input = 'Coba Coba'
        content.send_keys(status_input)
        submit.send_keys(Keys.RETURN)

        self.assertIn(status_input, selenium.page_source)
    
    def test_welcome_is_correct(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        welcome = selenium.find_element_by_tag_name('h2').text
        self.assertEqual(welcome, "Welcome to my page!")
    
    def test_hello_apa_kabar_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        title = selenium.find_element_by_tag_name('h1').text
        self.assertEqual(title, "Hello, Apa kabar?")

    def test_css_valid_for_btn(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        btnStyle = selenium.find_element_by_tag_name('button').get_attribute("style")
        self.assertEqual(btnStyle, "border: 1px;")

    def test_css_valid_for_hr(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        garisStyle = selenium.find_element_by_tag_name('hr').get_attribute("style")
        self.assertEqual(garisStyle, "border-color: red;")

class StatusFunctionalTestHeroku(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest,self).setUp()

    def tearDown(self):
        time.sleep(6)
        self.selenium.quit()
        super(StatusFunctionalTest,self).tearDown()

    def test_status_form_can_submit(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://dashboard.heroku.com/apps/ppwstory6')
        # find the form element
        content = selenium.find_element_by_name('content')
        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        status_input = 'Coba Coba'
        content.send_keys(status_input)
        submit.send_keys(Keys.RETURN)

        self.assertIn(status_input, selenium.page_source)
    
    def test_welcome_is_correct(self):
        selenium = self.selenium
        selenium.get('https://dashboard.heroku.com/apps/ppwstory6')
        welcome = selenium.find_element_by_tag_name('h2').text
        self.assertEqual(welcome, "Welcome to my page!")
    
    def test_hello_apa_kabar_exist(self):
        selenium = self.selenium
        selenium.get('https://dashboard.heroku.com/apps/ppwstory6')
        title = selenium.find_element_by_tag_name('h1').text
        self.assertEqual(title, "Hello, Apa kabar?")

    def test_css_valid_for_btn(self):
        selenium = self.selenium
        selenium.get('https://dashboard.heroku.com/apps/ppwstory6')
        btnStyle = selenium.find_element_by_tag_name('button').get_attribute("style")
        self.assertEqual(btnStyle, "border: 1px;")

    def test_css_valid_for_hr(self):
        selenium = self.selenium
        selenium.get('https://dashboard.heroku.com/apps/ppwstory6')
        garisStyle = selenium.find_element_by_tag_name('hr').get_attribute("style")
        self.assertEqual(garisStyle, "border-color: red;")










