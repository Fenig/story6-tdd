from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import StatusModel
from .forms import StatusForm
# Create your views here.

landing_page_content = "Hello, Apa kabar?"
greeting = "Welcome to my page!"
profile_name = "Gregorius Aprisunnea"
profile_npm = "1706039710"
profile_hobby = "Makan"
profile_education = "Universitas Indonesia"
profile_experience = "Expert in eating"


def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    table = StatusModel.objects.all()
    form = StatusForm()
    return render(request, 'index.html', {'status': table, 'form': form, 'landing_page_content' : landing_page_content, "greeting":greeting})

def profile(request):
    return render(request, 'profile.html', {'profile_name' : profile_name, 'profile_npm' : profile_npm, 'profile_hobby' : profile_hobby,
    'profile_education' : profile_education, 'profile_experience' : profile_experience})
